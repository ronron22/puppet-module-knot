class knot::params {

  case $::osfamily {
    'Debian': {
      apt::source { 'knot':
        location => 'https://deb.knot-dns.cz/knot/',
        repos    => 'main',
      }
    }
  }

}
